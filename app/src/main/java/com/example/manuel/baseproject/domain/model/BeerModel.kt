package com.example.manuel.baseproject.domain.model

class BeerModel(
        val id: Int,
        val name: String?,
        val tagline: String?,
        val image: String?,
        val abv: Double?
)