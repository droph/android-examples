package com.example.manuel.baseproject.vm.model

enum class AbvType {
    GREEN,
    ORANGE,
    RED
}