package com.example.manuel.baseproject.commons.utils.enums

enum class ResultType {
    ERROR,
    LOADING,
    SUCCESS,
    EMPTY_DATA
}
